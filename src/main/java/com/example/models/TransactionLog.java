package com.example.models;

import org.springframework.beans.factory.annotation.Required;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class TransactionLog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private double leftOperand;

    @NotNull
    private double rightOperand;

    @NotNull
    private double result;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "arithmetic_type")
    @NotNull
    private Arithmetic arithmeticType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLeftOperand() {
        return leftOperand;
    }

    public void setLeftOperand(double leftOperand) {
        this.leftOperand = leftOperand;
    }

    public double getRightOperand() {
        return rightOperand;
    }

    public void setRightOperand(double rightOperand) {
        this.rightOperand = rightOperand;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public Arithmetic getArithmeticType() {
        return arithmeticType;
    }

    public void setArithmeticType(Arithmetic arithmeticType) {
        this.arithmeticType = arithmeticType;
    }
}
