package com.example.models;

public enum Arithmetic {
    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION;
}
