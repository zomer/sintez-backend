package com.example.repositories;

import com.example.models.TransactionLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionLogsRepository extends JpaRepository<TransactionLog, Long> {
}
