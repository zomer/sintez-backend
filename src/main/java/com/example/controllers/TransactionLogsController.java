package com.example.controllers;

import com.example.models.TransactionLog;
import com.example.services.TransactionLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/logs")
public class TransactionLogsController {

    @Autowired
    TransactionLogsService transactionLogsService;

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes="application/json", headers = "content-type=application/x-www-form-urlencoded")
    public void createPerson(@RequestBody TransactionLog item) {
        transactionLogsService.addLog(item);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List findItems() {
        return transactionLogsService.getAll();
    }
}
