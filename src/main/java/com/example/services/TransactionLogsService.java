package com.example.services;

import com.example.models.TransactionLog;
import java.util.List;

public interface TransactionLogsService {
    TransactionLog addLog(TransactionLog log);
    List<TransactionLog> getAll();
    void delete(TransactionLog log);
    TransactionLog findOne(long id);
}
