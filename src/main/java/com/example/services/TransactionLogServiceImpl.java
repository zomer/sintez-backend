package com.example.services;

import com.example.models.TransactionLog;
import com.example.repositories.TransactionLogsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionLogServiceImpl implements TransactionLogsService {

    @Autowired
    TransactionLogsRepository transactionLogsRepository;

    @Override
    public TransactionLog addLog(TransactionLog log) {
        TransactionLog saveLog = transactionLogsRepository.saveAndFlush(log);
        return saveLog;
    }

    @Override
    public List<TransactionLog> getAll() {
        return transactionLogsRepository.findAll();
    }

    @Override
    public void delete(TransactionLog log) {
        transactionLogsRepository.delete(log);
    }

    @Override
    public TransactionLog findOne(long id) {
        return transactionLogsRepository.findOne(id);
    }
}
