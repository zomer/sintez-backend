CREATE TABLE IF NOT EXISTS `transaction_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `arithmetic_type` int(11) NOT NULL,
  `left_operand` double NOT NULL,
  `result` double NOT NULL,
  `right_operand` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `arithmetic` (
  `id` int(11),
  `name` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `transaction_log` ADD CONSTRAINT `arithmetic_type` FOREIGN KEY (`arithmetic_type`) REFERENCES arithmetic( `id` );


INSERT INTO arithmetic (id, name) VALUE (0 , 'ADDITION');
INSERT INTO arithmetic (id, name) VALUE (1 , 'SUBTRACTION');
INSERT INTO arithmetic (id, name) VALUE (2 , 'MULTIPLICATION');
INSERT INTO arithmetic (id, name) VALUE (3 , 'DIVISION');

INSERT INTO transaction_log (id, arithmetic_type, left_operand, result, right_operand) VALUE (1 , 1, 1, 3, 1);
INSERT INTO transaction_log (id, arithmetic_type, left_operand, result, right_operand) VALUE (2 , 2, 6, 1, 5);
